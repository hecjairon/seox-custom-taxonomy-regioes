<?php
/**
* Plugin Name: Taxonomia Regiões
* Plugin URI: https://seox.pageswp.com
* Description: Este plugin cria uma taxomia personalizada, modifica a URL para remover o slug da taxonomia suporta 2 terms como hierarquia 
* Version: 1.0.0
* Author: Hector Rondon
* Author URI: https://seox.pageswp.com
*/

defined( 'ABSPATH' ) or die( 'Silence is golden.!' );


include_once 'Controller/ClassRegisterTaxonomi.php';
include_once 'Controller/ClassRegisterRouteApi.php';

new ClassRegisterTaxonomi();

new ClassRegisterRouteApi();
