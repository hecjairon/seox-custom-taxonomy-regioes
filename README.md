# Teste Backend - SEOX

## Instalação 
    
    1. Clonar o projeto dentro do diretorio 'wp-content/plugins/': 
        git clone https://gitlab.com/hecjairon/seox-custom-taxonomy-regioes.git taxRegioes
    
    2. Ativar o Plugin : 
        Dashboard -> Plugins -> Plugins instalados > 'Taxonomia Regiões'

## Testes API Postman
    Os testes foram armazenados no seguinte workspace: 
        https://www.postman.com/spacecraft-architect-52296409/workspace/seox 

