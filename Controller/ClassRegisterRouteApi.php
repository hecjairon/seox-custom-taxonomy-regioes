<?php
class ClassRegisterRouteApi {

    public function __construct(){
        add_action( 'rest_api_init', [$this, 'registerApiRouteCustomTaxonomy'] );
    }

    public function registerApiRouteCustomTaxonomy() {
        register_rest_route('wp/v2', '/regioes/(?P<id>\d+)', [
            'methods'               => 'GET',
            'callback'              => [$this, 'getPostsByIdCustomTaxonomy'],
            'permission_callback'   => '__return_true',
            'args'                  => [
                                        'id'    => [
                                            'description'   => __('ID da região'),
                                            'type'          => 'integer',
                                        ],
                                    ],
        ]);
    }

    public function getPostsByIdCustomTaxonomy( $request ) {
        $region_id  = $request->get_param('id');
        $args       = [
                    'post_type' => 'post',
                    'tax_query' => [ [
                                        'taxonomy'  => 'regioes',
                                        'field'     => 'term_id',
                                        'terms'     => $region_id,
                                        ],
                                    ],
                    ];
        $query = new WP_Query($args);
        $posts = $query->get_posts();
        return rest_ensure_response( $posts );
    }
}
