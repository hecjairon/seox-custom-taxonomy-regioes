<?php
class ClassRegisterTaxonomi {

    public function __construct(){
        add_action('init', [$this, 'registerTaxonomyRegions'] );
        add_filter('post_link', [$this, 'customLinkTaxonomy'], 10, 3);
        add_action( 'init', [$this, 'rewriteRuleTaxonomyRegions'] );
        add_action( 'init', [$this, 'flushRewriteRules'] ); 
    }

    public function registerTaxonomyRegions() {
        $labels = [
            'name'              => _x( 'Região', 'taxonomy general name' ),
            'singular_name'     => _x( 'Região', 'taxonomy singular name' ),
            'search_items'      => __( 'Buscar elementos' ),
            'all_items'         => __( 'Todos los elementos' ),
            'parent_item'       => __( 'Elemento padre' ),
            'parent_item_colon' => __( 'Elemento padre:' ),
            'edit_item'         => __( 'Editar elemento' ),
            'update_item'       => __( 'Actualizar elemento' ),
            'add_new_item'      => __( 'Agregar nuevo elemento' ),
            'new_item_name'     => __( 'Nombre del nuevo elemento' ),
            'menu_name'         => __( 'Regiões' )
        ];

        $args = [
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'public'            => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => true,
            'show_in_rest'      => true,
            'rewrite'           => ['hierarchical'  => true,
                                    'slug'          => ''],
        ];
        register_taxonomy( 'regioes', ['post'], $args );
        register_taxonomy_for_object_type( 'regioes', 'post' );
    }

    public function customLinkTaxonomy($permalink, $post, $leavename) {
        $terms = wp_get_post_terms( $post->ID, 'regioes' );
        if ( ! empty( $terms ) ) {
            $term = $terms[0];
            $parent = $term->parent;
            if ( ! empty($parent) ) {
                $newUrl = get_term_link($parent, 'regioes', false) . $term->slug . '/noticias/' . $post->post_name;
            } else {
                $newUrl = get_term_link($term, 'regioes', false) .'noticias/'. $post->post_name;
            }
            $newUrl = str_replace('/regioes', '', $newUrl);
            $permalink = $newUrl;
        }else{
            $permalink = 'noticias/'. $post->post_name;
        }
        return $permalink;
    }

    public function rewriteRuleTaxonomyRegions() {
        add_rewrite_rule(
            '^([^/]+)/([^/]+)/noticias/([^/]+)/?$',
            'index.php?regioes=$matches[1]&term_slug=$matches[2]&name=$matches[3]',
            'top'
        );
        add_rewrite_rule(
            '^([^/]+)/noticias/([^/]+)/?$',
            'index.php?regioes=$matches[1]&name=$matches[2]',
            'top'
        );
        add_rewrite_rule(
            '^noticias/([^/]+)/?$',
            'index.php?name=$matches[1]',
            'top'
        );
        add_rewrite_rule(
            '^([^/]+)/([^/]+)/?$',
            'index.php?regioes=$matches[1]&term_slug=$matches[2]',
            'top'
        );
        add_rewrite_rule(
            '^([^/]+)/?$',
            'index.php?regioes=$matches[1]',
            'top'
        );
    }

    public function flushRewriteRules() {
        flush_rewrite_rules();
    }

}